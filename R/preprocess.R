#' takes presorted sample names, counts, and gene lengths and IDs,
#' constructs a usable data frame, filters for protein-coding genes only.
#' discards low-experssion genes, scales numeric data to kb and kbpm,
#' and performs variance-stabilizing transform (log transform)
#' returns the scaled, filtered, transformed data
#' @param gene_ids sorted list of canonical, unique gene ID
#' @param gene_lengths sorted list of lengths of gene sequences (basepairs, not kbasepairs)
#' @param raw_sample_counts sorted list or matrix of raw read counts
#' @param sample_group_names descriptive names of sample groups, sorted respectively
#' @param expression_cutoff minimum total number of reads for sample to be considered in analysis (in millions)
#' @return object with two attributes: filtered, scaled count_df and filtered, scaled, log xformed vst_df
#' @export preprocess


preprocess <- function(gene_ids, gene_lengths, raw_sample_counts, sample_group_names, expression_cutoff=3.0){
  # construct df of scaled values
  colnames(raw_sample_counts) <- sample_group_names
  rownames(raw_sample_counts) <- gene_ids
  reads_per_million <- raw_sample_counts/10e6
  gene_length_kbases <- gene_lengths/10e3
  count_df <- data.frame('Gene.ID'=gene_ids, 'Length'=gene_length_kbases, reads_per_million, stringsAsFactors=FALSE)
  
  # remove low expression genes
  rs <- rowSums(reads_per_million)
  count_df <- count_df[rs>3,]

  # filter for protein-coding genes
  load("../data/ensg2symbol2entrez.RData")
  count_df <-count_df[rownames(count_df) %in% ensg2symbol$ensembl_gene_id[ensg2symbol$gene_biotype=="protein_coding"],]
  
  # perform variance stabilizing transform
  vst_matrix <- log2(reads_per_million)
  vst_df <- data.frame(count_df$Gene.ID, count_df$Length, vst_matrix[rownames(count_df),], stringsAsFactors=FALSE)
  
  processed <- list()
  processed$vst_df <- vst_df
  processed$count_df <- count_df
  return(processed)
}